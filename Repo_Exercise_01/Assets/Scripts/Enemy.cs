﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CoAHomework;

public class Enemy
{
    public int health;
    public int enemyCount;
    public float attackSpeed;
    public float attackDamage;
    public string enemyName;
    public string enemyAttack;
    public string enemyWeapon;
    public bool damageTaken;
    public bool isEnemyAlive;
    public bool isEnemyPoisoned;
    public bool isEnemyImmortal;
}
